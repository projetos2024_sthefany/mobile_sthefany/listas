import React from "react";
import { useEffect, useState } from "react";
import { View, Text, FlatList } from "react-native";
import axios from "axios";


const UsersScreen = () => {
  const [users, setUsers] = useState([]);
  useEffect(() => {
    getUsers()
  }, []);

  async function getUsers() {
    try {
        const response = await axios.get('https://jsonplaceholder.typicode.com/users');//REQUISIÇÃO (VEM PARA A RESPONSE)
        //SOLICITAÇAO PARA A API DE FORA
        setUsers(response.data);
    } catch (error) {
        console.error(error)
    }
  }

  return (
    <View>
      <Text>Lista de Usuários</Text>
      <FlatList
        data={users}
        keyExtractor={(item) => item.id.toString()}
        renderItem={({ item }) => (
          <View>
            <Text>Nome: {item.name}</Text>
          </View>
        )}
      />
    </View>
  );
};

export default UsersScreen;
